"""This file provides multiple helper Python functions"""

from discord.utils import get

from operator import attrgetter


async def strip_id(context):
    return context.message.mentions[0].id

async def get_role(context, role_name):
    for role in context.message.guild.roles:
        if role.name.lower() == role_name.lower():
            return role
    #return get(context.message.guild.roles, name=role_name)

async def get_member(context, user_id):
    return get(context.message.guild.members, id=user_id)
