import aioredis
import aiopg

async def setup_redis(bot):
    bot.redis = await aioredis.create_redis_pool('redis://localhost')

async def setup_postgres_pool(bot):
    bot.pg_pool = await aiopg.create_pool(POSTGRES_AUTH)

# Prefix used for bot commands
BOT_PREFIX = ("v?", "v!")

# What plugins to load from the `plugins` directory
COGS = (
    'plugins.meme',
#    'plugins.roles_sub',
    'plugins.notifications',
    'plugins.loaders',
    'plugins.messages',
    'plugins.alt_checker',
    'plugins.lmgtfy',
    'plugins.qotd',
    'plugins.utilities',
)

# The number of messages discord.py stores in memory
MAX_MESSAGES = 5000

# Discord Token for bot authentication
TOKEN = ''

POSTGRES_AUTH = 'dbname=<db_name> user=<db_user> password=<db_password> host=127.0.0.1'

