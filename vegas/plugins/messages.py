from discord.ext import commands
from discord.utils import get

from .base import BasePlugin


class MessagesPlugin(BasePlugin):
    @commands.command(description="Edit a message based on an id",
                      case_insensitive=True)
    async def edit_message(self, context, message_id, message):
        caller_roles = [role.name for role in context.message.author.roles]
        if 'Admin' in caller_roles:
            old_message = await context.fetch_message(message_id)
            await old_message.edit(content=message)
            await context.message.delete()

    @commands.command(description="Send a message in a channel",
                      case_insensitive=True)
    async def send_message(self, context, message):
        caller_roles = [role.name for role in context.message.author.roles]
        if 'Admin' in caller_roles:
            await context.send(message)
            await context.message.delete()


def setup(bot):
    bot.add_cog(MessagesPlugin(bot))
