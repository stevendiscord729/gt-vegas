import functools
import datetime
import random

import discord
from discord.ext import commands

from discord.ext.commands import has_any_role

from .base import BasePlugin
from utils import get_role


ROLES = ["notif", "no", "deadlock", ]
COLOURS = [0xDE392E, 0xDF9326, 0xE6E13D, 0x37CB39, 0x3D9DCE, 0x5640D2, ]


class EmbedPlugin(BasePlugin):
    @commands.group(description="Embed Builder for Approveds!",
                    case_insensitive=True,
                    aliases=['e', ])
    async def embed(self, context):
        pass
        #await context.send('What do you want to do with the Embed Builder?')

    @embed.command(name='post',
                   description='Post an Embed',
                   brief='Post an Embed',
                   aliases=['p', ])
    @has_any_role('Admin', 'Can Embed')
    async def post(self, context, title=None, description=None, contact=None, dates=None, ping=None):
        color = context.message.author.top_role.color
        embed = discord.Embed(title=title, description=description, colour=color)
        embed.add_field(name='Contact', value=contact, inline=False)
        embed.add_field(name='Dates:', value=dates, inline=True)
        if context.message.attachments:
            embed.set_thumbnail(url=context.message.attachments[0].url)
        if ping:
            if any(vrole.lower() in ping.lower() for vrole in ROLES):
                role = await get_role(context, ping)
                await role.edit(mentionable=True)
                message = await context.send(role.mention, embed=embed)
                await role.edit(mentionable=False)
            else:
                message = await context.send("Sorry, you don't have permission to ping that role!")
        else:
            message = await context.send(embed=embed)
        await message.pin()


def setup(bot):
    bot.add_cog(EmbedPlugin(bot))
