"""This file provides multiple helper `checks` that make dealing with discord.py easier."""
import functools

import discord
from discord.ext import commands

def has_any_role(*required_roles):
    """has_any_role acts as a helper functon
    This function takes an iterable (usually a list) of discord role names.
    `predicate` loops through the list of roles and compares the list provided to the roles
    on the message author.
    This function returns `True` if the message author has a role in the provided list"""
    async def predicate(ctx):
        check_against_author_roles = functools.partial(discord.utils.get, ctx.author.roles)
        if any(check_against_author_roles(id=required_role) is not None if isinstance(required_role, int) else check_against_author_roles(name=required_role) is not None for required_role in required_roles):
            return True
        else:
            await ctx.send('Sorry it looks like only {} can use this role!'.format(required_roles))
    return discord.ext.commands.check(predicate)

def is_owner():
    """Helper function that returns true if you are SufiDarwish"""
    async def predicate(ctx):
        if ctx.author.id == 227117043143540736:
            return True
        await ctx.send('You dont have permission to use this!')
        return False
    return commands.check(predicate)
