# GT Discord Bot

This bot is built to be a utility bot for the [Official Game Theory Discord](https://discord.gg/gametheorist).

# Requirements

This project uses [Poetry](https://github.com/sdispater/poetry) to manage dependencies.

This project also uses Python 3.6 (tested 3.6.7), so you will need some version of Python 3.6 or higher as well. Getting this will depend on your OS, but [python.org](https://www.python.org/downloads/) should help

This project also uses Redis for Task Queues and Sub/Pub through `asyncio_redis`.

# Installation

Once you have forked and cloned this repo to your machine, you can setup the dependencies like so:

```
poetry install
```

Create a new `settings/local_settings.py` file with the following:

```
from .base import *

TOKEN = '<discord token>'

```

# Contributing

This bot uses discord.py so make sure you are familiar with that library. 

This project has four main sections:

## bot.py 

This file contains the VEGAS class that is the main class of our bot. The `__init__` method on `VEGAS` also contains the plugin loading logic as well as a custom `dm_author` method that demonstrates a simple bot command.

## run.py

This file is used to start the bot. It will import from your custom `settings.local_settings` (next section) for a `TOKEN` constant you provide as well as postgres information (check `settings.base` for more options). The rest of the functions are just simple helper methods and starting the main event loop.

## settings

The `settings` folder contains one file `base.py` and expects you to provide one file `local_settings.py`. `base.py` contains all settings for the bot that are not private (tokens) or personal (absolute file paths), that type of information should be placed in `settings/local_settings.py`. For the bot to start and work properly you must create and setup your own `local_settings` file. By default `local_settings` will be expected to provide a valid Discord Bot Token and aiopg connection string. These two settings should be stored in `TOKEN` and `POSTGRES_AUTH` respectively. Lastly `local_settings` will need to import all items in `settings.base`, this can be accomplished with `from .base import *`.

By default `settings/local_settings.py` is ignored by git.

## plugins

Where all plugin files are stored.

# Plugin Basics


`discord.py` provides a great method off of `discord.ext.commands.Bot` called `add_cog`. The `add_cog` method allows us to add all of the methods/commands from one class onto our bot. This method is greatly prefered as opposed to having to define every method/command in one file in one class. Breaking up our code like this keeps everything much more readable.

A sample of a simple plugin can be seen in `plugins/memes.py` and `plugins/notification.py`.

# Redis Setup

If you install redis and have it running on its default port 6379 vegas will connect fine. If you do not have redis or do not care about that functionality, simply remove asyncio_redis with `pip uninstall asyncio_redis` or remove those desired cogs from the settings file.

